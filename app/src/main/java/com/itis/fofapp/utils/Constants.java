package com.itis.fofapp.utils;

import com.vk.sdk.VKScope;

/**
 * Constants, which is used in this project
 */
public class Constants {

    // Constants for VK SDK
    public static final String APP_ID = "4939206";
    public static final String[] SCOPES = { VKScope.FRIENDS };
    public static final String FIELDS_REQUEST = "id,first_name,last_name,photo_50";

    // Constant for viewing friends of chosen friend
    public static final String BUNDLE_ID = "id";

    // Constants for saveInstanceState tags in FriendsFragment
    public static final String FRIENDS_TAG = "mFriends";
    public static final String VISIBLE_POSITION_TAG = "mVisiblePosition";
    public static final String OFFSET_TAG = "mOffset";
    public static final String PREVIOUS_TOTAL_TAG = "mPreviousTotal";

    // Constants for JSON in FriendsActivity
    public static final String JSON_RESPONSE = "response";
    public static final String JSON_ID = "id";
    public static final String JSON_FIRST_NAME = "first_name";
    public static final String JSON_LAST_NAME = "last_name";
    public static final String JSON_PHOTO = "photo_50";
}
