package com.itis.fofapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Utils {

    /**
     * Performs downloading of image from url
     * @param url url of source image
     * @return image - avatar of user
     */
    public static Bitmap getBitmapFromUrl(String url) {
        try {
            if (url != null) {
                URL imageUrl = new URL(url);
                return BitmapFactory.decodeStream(imageUrl.openConnection().getInputStream());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
