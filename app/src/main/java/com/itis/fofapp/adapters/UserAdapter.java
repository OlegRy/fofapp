package com.itis.fofapp.adapters;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.itis.fofapp.R;
import com.itis.fofapp.model.User;
import com.itis.fofapp.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Provides viewing of User objects in RecyclerView
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<User> mVisibleUsers;
    private List<User> mAllUsers;
    private OnItemClickListener mItemClickListener;

    public UserAdapter(List<User> visibleUsers) {
        mVisibleUsers = visibleUsers;
        mAllUsers = visibleUsers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_item,
                parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = mVisibleUsers.get(position);
        holder.tv_name.setText(user.getName());
        new DownloadImageTask(holder.iv_avatar).execute(user.getAvatar());
    }

    @Override
    public int getItemCount() {
        return mVisibleUsers.size();
    }

    /**
     * This interface provides clicking at
     * single row of RecyclerView
     */
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    /**
     * Resets filter
     */
    public void flushFilter() {
        mVisibleUsers = new ArrayList<>();
        mVisibleUsers.addAll(mAllUsers);
        notifyDataSetChanged();
    }

    /**
     * Filter list of RecyclerView
     * @param query filter string
     */
    public void setFilter(String query) {
        mVisibleUsers = new ArrayList<>();
        query = query.toLowerCase();
        for (User user : mAllUsers) {
            if (user.getName().toLowerCase().contains(query)) {
                mVisibleUsers.add(user);
            }
        }
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private CircleImageView iv_avatar;
        private TextView tv_name;

        public ViewHolder(View itemView) {
            super(itemView);
            iv_avatar = (CircleImageView) itemView.findViewById(R.id.iv_avatar);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    /**
     * Provides asynchronous loading of image from url
     */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        private WeakReference<CircleImageView> mWeakReference;

        public DownloadImageTask(CircleImageView iv_avatar) {
            mWeakReference = new WeakReference<>(iv_avatar);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            if (params.length > 0) {
                return Utils.getBitmapFromUrl(params[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            CircleImageView iv_avatar = mWeakReference.get();
            if (iv_avatar != null) {
                if (bitmap != null) {
                    iv_avatar.setImageBitmap(bitmap);
                } else {
                    iv_avatar.setImageResource(R.drawable.ic_launcher);
                }
            }

        }
    }

}