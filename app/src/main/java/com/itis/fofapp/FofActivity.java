package com.itis.fofapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.itis.fofapp.fragments.AutorizationFragment;
import com.itis.fofapp.utils.Constants;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKSdkListener;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKError;
import com.vk.sdk.dialogs.VKCaptchaDialog;

/**
 * This activity launches, when you launch application
 * first time or when you logout from your vk profile
 */
public class FofActivity extends AppCompatActivity {

    private VKSdkListener mVKSdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError vkError) {
            new VKCaptchaDialog(vkError).show(FofActivity.this);
        }

        @Override
        public void onTokenExpired(VKAccessToken vkAccessToken) {
            VKSdk.authorize(Constants.SCOPES);
        }

        @Override
        public void onAccessDenied(VKError vkError) {
            new AlertDialog.Builder(VKUIHelper.getTopActivity())
                    .setMessage(vkError.toString()).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fof);
        VKSdk.initialize(mVKSdkListener, Constants.APP_ID);
        VKUIHelper.onCreate(this);
        if (VKSdk.wakeUpSession()) {
            startFriendsActivity();
        } else {
            showAutorizationFragment(new AutorizationFragment());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            startFriendsActivity();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    private void showAutorizationFragment(AutorizationFragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, fragment).commit();
    }

    private void startFriendsActivity() {
        startActivity(new Intent(this, FriendsActivity.class));
        finish();
    }
}
