package com.itis.fofapp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Model for viewing in RecyclerView and
 * navigation drawer
 */
public class User implements Parcelable {

    private int mId;
    private String mName;
    private String mAvatar;

    public User() {

    }

    public User(int id, String name, String avatar) {
        mId = id;
        mName = name;
        mAvatar = avatar;
    }


    public String getAvatar() {
        return mAvatar;
    }

    public void setAvatar(String avatar) {
        mAvatar = avatar;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeString(mAvatar);
    }
}
