package com.itis.fofapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.itis.fofapp.R;
import com.itis.fofapp.utils.Constants;
import com.vk.sdk.VKSdk;

/**
 * This fragment allows user to authorize
 * via Vk SDK
 */
public class AutorizationFragment extends Fragment {

    private Button btn_autorize;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_autorization, container, false);

        btn_autorize = (Button) view.findViewById(R.id.btn_autorize);
        btn_autorize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VKSdk.authorize(Constants.SCOPES);
            }
        });

        return view;
    }
}
