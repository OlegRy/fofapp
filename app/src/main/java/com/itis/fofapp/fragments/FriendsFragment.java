package com.itis.fofapp.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.itis.fofapp.R;
import com.itis.fofapp.adapters.UserAdapter;
import com.itis.fofapp.model.User;
import com.itis.fofapp.utils.Constants;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Shows list of friends for user with
 * specified id (or authorized user if there is no id)
 */
public class FriendsFragment extends Fragment implements UserAdapter.OnItemClickListener {

    private RecyclerView rv_friends;
    private UserAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    private List<User> mFriends;
    private VKRequest mVKRequest;
    private long mUserId;
    private int mOffset;
    private boolean mLoading = true;
    private int mVisibleThreshold = 10;
    private int mPreviousTotal;
    private int mVisisblePosition;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (savedInstanceState != null) {
            mFriends = savedInstanceState.getParcelableArrayList(Constants.FRIENDS_TAG);
            mVisisblePosition = savedInstanceState.getInt(Constants.VISIBLE_POSITION_TAG);
            mOffset = savedInstanceState.getInt(Constants.OFFSET_TAG);
            mPreviousTotal = savedInstanceState.getInt(Constants.PREVIOUS_TOTAL_TAG);
        } else {
            mVisisblePosition = 0;
            mFriends = new ArrayList<>();
            mOffset = 0;
            mPreviousTotal = 0;
        }

        if (args != null) {
            mUserId = args.getInt(Constants.BUNDLE_ID, -1);
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friends, container, false);
        rv_friends = (RecyclerView) view.findViewById(R.id.rv_friends);
        mAdapter = new UserAdapter(mFriends);
        mAdapter.setItemClickListener(this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_friends.setLayoutManager(mLayoutManager);

        rv_friends.setAdapter(mAdapter);
        rv_friends.getLayoutManager().scrollToPosition(mVisisblePosition);
        rv_friends.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                if (mLoading) {
                    if (totalItemCount > mPreviousTotal) {
                        mLoading = false;
                        mPreviousTotal = totalItemCount;
                    }
                }
                if (!mLoading && (totalItemCount - visibleItemCount)
                        <= (pastVisiblesItems + mVisibleThreshold)) {
                    mOffset += 10;
                    loadFriends(mOffset);
                    mLoading = true;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
        if (mFriends.isEmpty()) loadFriends(0);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(Constants.FRIENDS_TAG, (ArrayList<? extends Parcelable>) mFriends);
        outState.putInt(Constants.VISIBLE_POSITION_TAG, mLayoutManager.findFirstVisibleItemPosition());
        outState.putInt(Constants.OFFSET_TAG, mOffset);
        outState.putInt(Constants.PREVIOUS_TOTAL_TAG, mPreviousTotal);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onItemClick(View view, int position) {
        Fragment fragment = new FriendsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.BUNDLE_ID, mFriends.get(position).getId());
        fragment.setArguments(args);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_friends_fragment, menu);
        SearchManager manager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        MenuItem item = menu.findItem(R.id.me_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setSearchableInfo(manager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint(getResources().getString(R.string.action_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    mAdapter.flushFilter();
                } else {
                    mAdapter.setFilter(newText);
                }

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Loads friends of user with specified id
     * (or of authorized user if there is no id)
     * @param offset provides loading of friends
     *               by portions, when you scroll
     *               list
     */
    private void loadFriends(final int offset) {
        if (mVKRequest != null) mVKRequest.cancel();

        if (offset == 0) mFriends.clear();

        if (mUserId == -1) {
            mVKRequest = VKApi.friends().get(VKParameters.from(VKApiConst.COUNT, 10,
                    VKApiConst.OFFSET, offset,
                    VKApiConst.FIELDS, Constants.FIELDS_REQUEST));
        } else {
            mVKRequest = VKApi.friends().get(VKParameters.from(VKApiConst.USER_ID, mUserId,
                    VKApiConst.COUNT, 10, VKApiConst.OFFSET, offset,
                    VKApiConst.FIELDS, Constants.FIELDS_REQUEST));
        }
        mVKRequest.executeWithListener(new VKRequest.VKRequestListener() {

            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKUsersArray usersArray = (VKUsersArray) response.parsedModel;
                if (offset < usersArray.getCount()) {
                    fillFriends(usersArray);
                }
                mAdapter.notifyDataSetChanged();
            }

        });

    }

    /**
     * Fills list of friends
     * @param usersArray model, which is received from VK
     */
    private void fillFriends(VKUsersArray usersArray) {
        for (VKApiUserFull vkApiUserFull : usersArray) {
            if (!vkApiUserFull.is_deleted) {
                mFriends.add(new User(vkApiUserFull.id, vkApiUserFull.first_name + " " +
                        vkApiUserFull.last_name, vkApiUserFull.photo_50));
            }
        }
    }
}
