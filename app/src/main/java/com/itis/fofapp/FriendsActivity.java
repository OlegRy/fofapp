package com.itis.fofapp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.itis.fofapp.fragments.FriendsFragment;
import com.itis.fofapp.model.User;
import com.itis.fofapp.utils.Constants;
import com.itis.fofapp.utils.Utils;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.vk.sdk.VKSdk;
import com.vk.sdk.VKUIHelper;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This activity launches, when you authorize through
 * Vk SDK. It shows list of authorized user and a little
 * bit of information about current user such as avatar,
 * name and surname. Also this activity allows you to
 * logout and then you can authorize again
 */
public class FriendsActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private User mCurrentUser;
    private VKRequest mVKRequest;
    private Drawer mDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        VKUIHelper.onCreate(this);
        mToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        loadCurrentUser();
        showFriends();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        VKUIHelper.onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Shows list of friends
     */
    private void showFriends() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (findViewById(R.id.container) != null) {
            transaction.replace(R.id.container, new FriendsFragment());
        } else {
            transaction.add(R.id.container, new FriendsFragment());
        }
        transaction.commit();
    }


    /**
     * Loads some information about authorized user
     */
    private void loadCurrentUser() {
        if (mVKRequest != null) mVKRequest.cancel();
        mCurrentUser = new User();
        mVKRequest = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "photo_50"));
        mVKRequest.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                try {
                    fillUser(response.json.getJSONArray(Constants.JSON_RESPONSE));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    /**
     * Method for filling user object
     * @param userJson information about user from Vk
     * @throws JSONException
     */
    private void fillUser(JSONArray userJson) throws JSONException {
        JSONObject currentJSON = userJson.getJSONObject(0);
        mCurrentUser.setId(currentJSON.getInt(Constants.JSON_ID));
        mCurrentUser.setName(currentJSON.getString(Constants.JSON_FIRST_NAME) + " "
                + currentJSON.getString(Constants.JSON_LAST_NAME));
        mCurrentUser.setAvatar(currentJSON.getString(Constants.JSON_PHOTO));
        new DownloadImageTask().execute();
    }

    /**
     * Initializes navigation drawer with some information
     * about authorized user. Also it provides possibility
     * to look friends of authorized user from another user's
     * list of friends
     * @param bitmap avatar of authorized user
     */
    private void initDrawer(Bitmap bitmap) {
        AccountHeader header = new AccountHeaderBuilder()
                .withActivity(FriendsActivity.this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName(mCurrentUser.getName()).withIcon(bitmap)
                ).build();
        mDrawer = new DrawerBuilder()
                .withActivity(FriendsActivity.this)
                .withToolbar(mToolbar)
                .withAccountHeader(header)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.my_friends),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.logout)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> adapterView, View view, int i, long l, IDrawerItem iDrawerItem) {
                        switch (i) {
                            case 0:
                                showFriends();
                                break;
                            case 2:
                                VKSdk.logout();
                                startActivity(new Intent(FriendsActivity.this, FofActivity.class));
                                finish();
                                break;
                        }
                        mDrawer.closeDrawer();
                        return true;
                    }
                })
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View view) {
                        hideKeyboard();
                    }

                    @Override
                    public void onDrawerClosed(View view) {

                    }

                    @Override
                    public void onDrawerSlide(View view, float v) {

                    }
                }).build();
    }

    /**
     * hides keyboard, when user open navigation drawer
     */
    private void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) FriendsActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(FriendsActivity.this.getCurrentFocus().getWindowToken(), 0);
    }

    /**
     * Performs asynchronous loading of image from url
     */
    class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            return Utils.getBitmapFromUrl(mCurrentUser.getAvatar());
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            initDrawer(bitmap);
        }
    }
}
